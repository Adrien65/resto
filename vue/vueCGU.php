<p> <!-- Conditions générales d'utilisation -->
    <h1>Conditions Générales d’Utilisation</h1>
    Les présentes conditions générales régissent l’utilisation de ce site <em>resto.fr</em>. </br>
    Ce site appartient et est géré par CIOBANU Adrien. </br>
    En utilisant ce site, vous indiquez que vous avez lu et compris les conditions d’utilisation et que vous acceptez de les respecter en tout temps. </br>
    </br>
    Type de site : site web de critique de restaurants.
</p>
<p> <!-- Contribution d'utilisateur -->
    <h2>Contribution d’utilisateur </h2>
    Les utilisateurs peuvent publier les informations suivantes sur notre site :
    <ul>
        <li>Lire une description du restaurant.</li>
        <li>Contacter le restaurant grâce aux coordonnées présentes.</li>
        <li>Avoir un aperçu des lieux grâce aux photos présentes.</li>
        <li>Commentaire du public</li>
    </ul>
    En affichant publiquement sur notre site, vous acceptez de ne pas agir illégalement ou violer les conditions d’utilisation acceptable énumérées dans ce document. </br>
</p>
<p> <!-- Comptes -->
    <h2>Comptes</h2>
    Lorsque vous créez un compte sur notre site, vous acceptez ce qui suit :
    <ol>
        <li>que vous êtes seul responsable de votre compte et de la sécurité et la confidentialité de votre compte, y compris les mots de passe ou les renseignements de nature 
            délicate joints à ce compte.</li>
        <li>que tous les renseignements personnels que vous nous fournissez par l’entremise de votre compte sont à jour, exacts et véridiques et que vous mettrez à jour vos 
            renseignements personnels s’ils changent.</li>
    </ol>
    Nous nous réservons le droit de suspendre ou de résilier votre compte si vous utilisez notre site illégalement ou si vous violez les conditions d’utilisation acceptable.
</p>
<p> <!-- Lois applicables -->
    <h2>Lois applicables</h2>
    Ce document est soumis aux lois applicables en France et vise à se conformer à ses règles et règlements nécessaires. Cela inclut la réglementation à l’échelle de l’UE 
    énoncée dan le RGPD (Règlement Général sur la Protection des Données).
</p>
<p> <!-- Divisibilité -->
    <h2>Divisibilité</h2>
    Si, à tout moment, l’une des dispositions énoncées dans le présent document est jugée incompatible ou invalide en vertu des lois applicables, ces dispositions seront considérées 
    comme nulles et seront retirées du présent document. Toutes les autres dispositions ne seront pas touchées par les lois et le reste du document sera toujours considéré comme 
    valide.
</p>
<p> <!-- Modifications -->
    <h2>Modifications</h2>
    Ces conditions générales peuvent être modifiées de temps à autre afin de maintenir le respect de la lois et de refléter tout changement à la façon dont nous gérons notre site et 
    la façon dont nous nous attendons à ce que les utilisateurs se comportent sur notre site. Nous recommandons à nos utilisateurs de vérifier ces conditions générales de temps à 
    autre pour s’assurer qu’ils sont informés de toute mise à jour. Au besoin, nous informerons les utilisateurs par courriel des changements apportés à ces conditions où nous 
    afficherons un avis sur notre site.
</p>
<p> <!-- Contact -->
    <h2>Contact</h2>
    Veillez communiquer avec nous si vous avez des questions ou des préoccupations. Nos coordonnées sont les suivantes :
    <li>01 23 45 67 89</li>
    <li>resto@gmail.com</li>
</p>
